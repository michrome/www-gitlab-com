---
layout: markdown_page
title: "Category Vision - Maven Repository"
---

- TOC
{:toc}

## Maven Repository

An easy to use integration of a package management tool, like Maven for Java developers, provides a standardized way to share and version control these types libraries across projects.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Maven%20Repository)
- [Overall Vision](https://about.gitlab.com/direction/package/)
- [UX Research](https://gitlab.com/groups/gitlab-org/-/epics/593)

## What's Next & Why

TODO

## Competitive Landscape

TODO

## Top Customer Success/Sales Issue(s)

TODO

## Top Customer Issue(s)

TODO

## Top Internal Customer Issue(s)

TODO

## Top Vision Item(s)

TODO
