---
layout: markdown_page
title: "IAM.3.02 - Source Code Security Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# IAM.3.02 - Source Code Security

## Control Statement

Access to modify source code is restricted to authorized personnel.

## Context

As GitLab is open source and we have contributors outside of the company from across the world, anybody can view and submit edits to the codebase to fix issues, add features, and so on. The spirit of this control is to ensure there's a process in place for all additions, including from the GitLab community, are appropriately reviewed and approved before being merged into the codebase.

## Scope

This control applies to any system or process where source code can be modified.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLabbers, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.3.02_source_code_security.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.3.02_source_code_security.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/IAM.3.02_source_code_security.md).

## Framework Mapping

* ISO
  * A.9.4.5
* SOC2 CC
  * CC8.1
