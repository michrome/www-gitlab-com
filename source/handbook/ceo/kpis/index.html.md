---
layout: markdown_page
title: "E-group KPIs"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Intro

Every department at GitLab has Key Performance Indicators (KPIs).
A department's KPIs are owned by the respective member of e-group.
This page has the goals and links to the definitions.
The goals are merged by the CEO.
The KPI definition should be in the most relevant part of the handbook which is organized by [function and results](/handbook/handbook-usage/#style-guide-and-information-architecture).
For example 'Wider community contributions per release' should be in the Community Releations part of the handbook.
In the definition it should mention what the canonical source is for this indicator.

## Public

In the doc 'GitLab Metrics' at IPO are the KPIs that we may share publicly.

## GitLab KPIs

GitLab goals are duplicates of goals of the reports.
GitLab goals are the most important indicators of company performance.

1. [IACV](/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv) vs. plan > 1
1. TCV - OpEx vs. plan > 1
1. [Sales efficiency ratio](/handbook/finance/operating-metrics/#sales-efficiency-ratio) > 1
1. Pipe generated vs. plan > 1
1. Wider community contributions per release
1. [LTV / CAC](/handbook/finance/operating-metrics/#ltv-to-cac-ratio) ratio > 4
1. Average NPS
1. Hires vs. plan > 0.9
1. Monthly employee turnover
1. New hire average score
1. Merge Requests per release per developer
1. Uptime GitLab.com
1. Active users per hosting platform: Total, AWS, Azure, GCP, IBM, Unknown
1. [Support CSAT](https://about.gitlab.com/handbook/finance/operating-metrics/#csat)
1. Runway > 12 months
1. [MAUI](http://www.meltano.com/docs/roadmap.html#maui) (Meltano so not part of the GitLab Executive Team KPIs) > 10% WoW

## Sales KPIs

1. [IACV](https://about.gitlab.com/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv) vs. plan > 1
1. [Field efficiency ratio](/handbook/finance/operating-metrics/#field-efficiency-ratio) > 2
1. [TCV](https://about.gitlab.com/handbook/finance/operating-metrics/#total-contract-value-tcv) vs. plan > 1
1. [ARR](https://about.gitlab.com/handbook/finance/operating-metrics/#annual-recurring-revenue-arr) YoY > 190%
1. Win rate > 30%
1. % of ramped reps at or above quota > 0.7
1. [Net Retention](https://about.gitlab.com/handbook/finance/operating-metrics/#retention-gross--net-dollar-weighted) > 2
1. [Gross Retention](https://about.gitlab.com/handbook/finance/operating-metrics/#retention-gross--net-dollar-weighted) > 0.9
1. Rep IACV per comp > 5
1. [ProServe](/handbook/finance/operating-metrics/#pcv) revenue vs. cost > 1.1
1. Services attach rate for strategic > 0.8
1. Self-serve sales ratio > 0.3
1. Licensed users
1. [ARPU](/handbook/finance/operating-metrics/#arpu)
1. New strategic accounts
1. [IACV per Rep](/handbook/finance/operating-metrics/#iacv-rep) > $1.0M
1. New hire location factor < TBD

## Marketing KPIs

1. Pipe generated vs. plan > 1
1. Pipe-to-spend > 5
1. [Marketing efficiency ratio](/handbook/finance/operating-metrics/#marketing-efficiency-ratio) > 2
1. SCLAU
1. [LTV / CAC ratio](https://about.gitlab.com/handbook/finance/operating-metrics/#ltv-to-cac-ratio) > 4
1. Twitter mentions
1. Sessions on our marketing site
1. New users
1. Product Installations: Download, start of installation, success installation, created admin user, configured email, second user invited, 30 day active, updates
1. Social response time
1. Meetup Participants with GitLab presentation
1. GitLab presentations given
1. Wider community contributions per release
1. Monthly Active Contributors from the wider community
1. New hire location factor < TBD

## People Operations KPIs

1. Hires vs. plan > 0.9
1. Apply to hire days < 30
1. No offer [NPS](/handbook/finance/operating-metrics/#nps) > [4.1](https://stripe.com/atlas/guides/scaling-eng)
1. Offer acceptance rate > 0.9
1. Average [NPS](/handbook/finance/operating-metrics/#nps)
1. Average location factor
1. New hire location factor < TBD
1. 12 month employee turnover < 16%
1. Voluntary employee turnover < 10%
1. Candidates per vacancy
1. Percentage of vacancies with active sourcing
1. New hire average score
1. Onboarding [NPS](/handbook/finance/operating-metrics/#nps)
1. Diversity lifecycle: applications, recruited, interviews, offers, acceptance, retention
1. PeopleOps cost per employee
1. [Discretionary bonus](/handbook/incentives/#discretionary-bonuses) per employee per month > 0.1 

## Finance KPIs

1. [IACV](/handbook/finance/operating-metrics/#incremental-annual-contract-value-iacv) per [capital consumed](/handbook/finance/operating-metrics/#capital-consumption) > 2
1. [Sales efficiency](/handbook/finance/operating-metrics/#sales-efficiency-ratio) > 1.0
1. [Magic number](https://about.gitlab.com/handbook/finance/operating-metrics/#magic-number) > 1.1
1. [Gross margin](/handbook/finance/operating-metrics/gross-margin) > 0.9
1. [Average days of sales outstanding](https://about.gitlab.com/handbook/finance/operating-metrics/#days-sales-outstanding-dso) < 45
1. [Average days to close](/handbook/finance/operating-metrics/#days-to-close) < 10
1. Runway > 12 months
1. New hire location factor < 0.7
1. ARR by annual cohort
1. Reasons for churn
1. Reasons for net expansion
1. Refunds processed as % of orders


## Product KPIs

1. [Ambition Percentage](https://about.gitlab.com/direction/#how-we-plan-releases) (Merged Issues/Planned Issues per Release) ~ 70%
1. SMAU
1. [MAU](/handbook/finance/operating-metrics/#monthly-active-user-mau)
1. Stage Monthly Active Instances (SMAI)
1. Monthly Active Instances (MAI)
1. [Sessions on release post](/handbook/finance/operating-metrics/#sessions-release-post)
1. Installation churn
1. User churn
1. Lost Instances
1. [Acquisition](https://about.gitlab.com/direction/growth/#acquistion)
1. [Adoption](https://about.gitlab.com/direction/growth/#adoption)
1. [Upsell](https://about.gitlab.com/direction/growth/#upsell)
1. [Rention](https://about.gitlab.com/direction/growth/#retention)
1. Net Promotor Score / [Customer Satisfaction](/handbook/finance/operating-metrics/#csat) with the product
1. New hire location factor < TBD
1. Signups
1. Onboarding completion rate
1. Comments
1. Accepting Merge Requests issue growth

## Engineering KPIs

1. Merge Requests per release per engineer in product development > 10
1. Uptime GitLab.com > 99.95%
1. Performance GitLab.com
1. Support [SLA](/handbook/finance/operating-metrics/#sla)
1. Support [CSAT](https://about.gitlab.com/handbook/finance/operating-metrics/#csat)
1. Support cost vs. recurring revenue
1. Days to fix S1 security issues
1. Days to fix S2 security issues
1. Days to fix S3 security issues
1. GitLab.com infrastructure cost per MAU
1. [ARR](/handbook/finance/operating-metrics/#annual-recurring-revenue-arr) per support rep > $1.175M
1. New hire location factor < 0.5
1. Public Cloud Spend

## Alliances KPIs

1. Active users per hosting platform: Total, AWS, Azure, GCP, IBM, Unknown
1. Active installations per hosting platform: Total, AWS, Azure, GCP, IBM, Unknown
1. Product Downloads: Updates & Initial per distribution method: Omnibus, Cloud native helm chart, Source
1. Acquistion velocity: [Acquire 3 teams per quarter](/handbook/alliances/acquisition-offer/) for less than $2m in total.
1. Acquistion success: 70% of acquisitions ship the majority of their old product functionality as part of GitLab within 3 months after acquistion.invited, 30 day active, updates
